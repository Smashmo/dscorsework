package set10106.rpc;
/*
 * Automatically generated by jrpcgen 1.0.7 on 03/02/14 15:23
 * jrpcgen is part of the "Remote Tea" ONC/RPC package for Java
 * See http://remotetea.sourceforge.net for details
 */
/**
 * A collection of constants used by the "Account" ONC/RPC program.
 */
public interface Account {
    public static final int ACCNT_VERS = 1;
    public static final int balance_1 = 1;
    public static final int deposit_1 = 2;
    public static final int ACCNT_PROG = 0x3fefeffe;
}
// End of Account.java
