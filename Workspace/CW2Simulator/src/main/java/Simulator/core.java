package Simulator;


import java.util.Date;

import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;

import ewsAlgorithm.*;

@WebService(serviceName = "main")
public class core{

	@WebMethod(operationName = "pole")
	public String pole (){
		return "Sim:OK";
	}
	
	@WebMethod(operationName = "getStats")
	public EWSBean getStats(@WebParam(name = "stBean")StatBean stBean,@WebParam(name = "si") int si) {
		
		
		EWSBean ewsBean = new EWSBean();
		ewsBean.setBloodPresh(stBean.getBloodPresh());
		ewsBean.setHartRate(stBean.getHartRate());
		ewsBean.setO2Sat(stBean.getO2Sat());
		ewsBean.setRespRate(stBean.getRespRate());
		ewsBean.setTemp(stBean.getTemp());
		
		//when timer expiers return the bean
		try {
			Thread.sleep(si);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return ewsBean;
		
	}
	
	private String getDateString() {
		Date now = new Date();
		return now.toString();
		}

}