package ewsAlgorithm;

public class StatBean {
	//Java bean for the stats that are in the file
	private int RespRate;
	private int O2Sat;
	private double Temp;
	private int BloodPresh;
	private int HartRate;
	
	public int getRespRate() {
		return RespRate;
	}
	public void setRespRate(int respRate) {
		RespRate = respRate;
	}
	public int getO2Sat() {
		return O2Sat;
	}
	public void setO2Sat(int o2Sat) {
		O2Sat = o2Sat;
	}
	public double getTemp() {
		return Temp;
	}
	public void setTemp(double temp) {
		Temp = temp;
	}
	public int getBloodPresh() {
		return BloodPresh;
	}
	public void setBloodPresh(int bloodPresh) {
		BloodPresh = bloodPresh;
	}
	public int getHartRate() {
		return HartRate;
	}
	public void setHartRate(int hartRate) {
		HartRate = hartRate;
	}

}
