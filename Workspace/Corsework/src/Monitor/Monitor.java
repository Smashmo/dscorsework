package Monitor;

import java.io.*;
import systemsAnalyser.*; //analyser is instansated in the menu
import javax.swing.*;

public class Monitor extends javax.swing.JFrame {
    // Variables for gui                  
    private javax.swing.JLabel BloodPreshLbl;
    private javax.swing.JLabel HartRateLbl;
    private javax.swing.JLabel O2Lbl;
    private javax.swing.JLabel RespRateLbl;
    private javax.swing.JLabel bpLblStat;
    private javax.swing.JMenuItem ei0;
    private javax.swing.JMenuItem ei10;
    private javax.swing.JMenuItem ei30;
    private javax.swing.JMenuItem ei5;
    private javax.swing.JMenu file;
    private javax.swing.JMenuItem fileOpen;
    private javax.swing.JMenuItem fileQuit;
    private javax.swing.JTextArea fileTextArea;
    private javax.swing.JLabel hrLblStat;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollFile;
    private javax.swing.JTextPane jTextPath;
    private javax.swing.JPanel jpanStats;
    private javax.swing.JMenuBar manuBar;
    private javax.swing.JFileChooser monFileOpen;
    private javax.swing.JLabel o2LblStat;
    private javax.swing.JMenu optEI;
    private javax.swing.JMenu optSI;
    private javax.swing.JMenu options;
    private javax.swing.JLabel rrLblStat;
    private javax.swing.JCheckBoxMenuItem showAnalyser;
    private javax.swing.JMenuItem si0;
    private javax.swing.JMenuItem si10;
    private javax.swing.JMenuItem si15;
    private javax.swing.JMenuItem si5;
    private javax.swing.JButton startBtn;
    private javax.swing.JButton stopBtn;
    private javax.swing.JLabel tempLbl;
    private javax.swing.JLabel tempLblStat;
    private javax.swing.JMenu view;
    private javax.swing.JTextField jTFFile;
    private javax.swing.JLabel scoreLbl;
    private javax.swing.JLabel scoreLblStat;
    private javax.swing.JLabel sensersLblStat;
    private javax.swing.JLabel sensersUpdateLbl;
    private java.awt.Canvas ewsCanvas;
    private javax.swing.JLabel ewsColourLbl;
    private javax.swing.JLabel ewsLblStat;
    private javax.swing.JLabel ewsUpdateLbl;


    JFileChooser fc;  //diclare the file open dialog
    File paitentFile; //file to be pased and displayed
    int RUNNING = 0; //used to check if the thread is running or not for the sim
    Thread runnerT;  //thead for the patent sim
    CW1MonWorker runner; //methrods to be run in the thread
    int SI = 1000; //1 second Default
    int EI = 60000; //1 minut


    public Monitor() {
        initComponents();  //all gui elements instansated in this methord
    }
       
    private void initComponents() {
    	//init all gui widgets, at more if neaded
        monFileOpen = new javax.swing.JFileChooser();
        jpanStats = new javax.swing.JPanel();
        RespRateLbl = new javax.swing.JLabel();
        O2Lbl = new javax.swing.JLabel();
        tempLbl = new javax.swing.JLabel();
        BloodPreshLbl = new javax.swing.JLabel();
        HartRateLbl = new javax.swing.JLabel();
        rrLblStat = new javax.swing.JLabel();
        o2LblStat = new javax.swing.JLabel();
        tempLblStat = new javax.swing.JLabel();
        bpLblStat = new javax.swing.JLabel();
        hrLblStat = new javax.swing.JLabel();
        jScrollFile = new javax.swing.JScrollPane();
        fileTextArea = new javax.swing.JTextArea();
        jPanel1 = new javax.swing.JPanel();
        scoreLbl = new javax.swing.JLabel();
        scoreLblStat = new javax.swing.JLabel();
        ewsLblStat = new javax.swing.JLabel();
        sensersLblStat = new javax.swing.JLabel();
        ewsUpdateLbl = new javax.swing.JLabel();
        sensersUpdateLbl = new javax.swing.JLabel();
        ewsColourLbl = new javax.swing.JLabel();
        ewsCanvas = new java.awt.Canvas();
        jTextPath = new javax.swing.JTextPane();
        startBtn = new javax.swing.JButton();
        stopBtn = new javax.swing.JButton();
        manuBar = new javax.swing.JMenuBar();
        file = new javax.swing.JMenu();
        fileOpen = new javax.swing.JMenuItem();
        fileQuit = new javax.swing.JMenuItem();
        options = new javax.swing.JMenu();
        optSI = new javax.swing.JMenu();
        si0 = new javax.swing.JMenuItem();
        si5 = new javax.swing.JMenuItem();
        si10 = new javax.swing.JMenuItem();
        si15 = new javax.swing.JMenuItem();
        optEI = new javax.swing.JMenu();
        ei0 = new javax.swing.JMenuItem();
        ei5 = new javax.swing.JMenuItem();
        ei10 = new javax.swing.JMenuItem();
        ei30 = new javax.swing.JMenuItem();
        view = new javax.swing.JMenu();
        showAnalyser = new javax.swing.JCheckBoxMenuItem();
        jTFFile = new javax.swing.JTextField();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Monitor");

        jpanStats.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        RespRateLbl.setText("Resparation Rate");

        O2Lbl.setText("Oxygen Level");

        tempLbl.setText("Temprature");

        BloodPreshLbl.setText("Blood Preshure");

        HartRateLbl.setText("Hart Rate");

        rrLblStat.setText("Null");

        o2LblStat.setText("Null");

        tempLblStat.setText("Null");

        bpLblStat.setText("Null");

        hrLblStat.setText("Null");

        javax.swing.GroupLayout jpanStatsLayout = new javax.swing.GroupLayout(jpanStats);
        jpanStats.setLayout(jpanStatsLayout);
        jpanStatsLayout.setHorizontalGroup(
            jpanStatsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jpanStatsLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jpanStatsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jpanStatsLayout.createSequentialGroup()
                        .addComponent(RespRateLbl)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 93, Short.MAX_VALUE)
                        .addComponent(rrLblStat))
                    .addGroup(jpanStatsLayout.createSequentialGroup()
                        .addComponent(HartRateLbl)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(hrLblStat))
                    .addGroup(jpanStatsLayout.createSequentialGroup()
                        .addComponent(tempLbl)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(tempLblStat))
                    .addGroup(jpanStatsLayout.createSequentialGroup()
                        .addComponent(O2Lbl)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(o2LblStat))
                    .addGroup(jpanStatsLayout.createSequentialGroup()
                        .addComponent(BloodPreshLbl)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(bpLblStat)))
                .addContainerGap())
        );
        jpanStatsLayout.setVerticalGroup(
            jpanStatsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jpanStatsLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jpanStatsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(RespRateLbl)
                    .addComponent(rrLblStat))
                .addGap(18, 18, 18)
                .addGroup(jpanStatsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(O2Lbl)
                    .addComponent(o2LblStat))
                .addGap(16, 16, 16)
                .addGroup(jpanStatsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(tempLbl)
                    .addComponent(tempLblStat))
                .addGap(18, 18, 18)
                .addGroup(jpanStatsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(BloodPreshLbl)
                    .addComponent(bpLblStat))
                .addGap(18, 18, 18)
                .addGroup(jpanStatsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(HartRateLbl)
                    .addComponent(hrLblStat))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        fileTextArea.setEditable(false);
        fileTextArea.setColumns(40);
        fileTextArea.setRows(5);
        jScrollFile.setViewportView(fileTextArea);

        jPanel1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        
        scoreLbl.setText("Curent EWS Score");

        scoreLblStat.setText("Null");

        ewsLblStat.setText("1 Minut (Default)");

        sensersLblStat.setText("1 Second (Default)");
        
        ewsUpdateLbl.setText("EWS Update EI");

        sensersUpdateLbl.setText("Sensers Update SI");

        ewsColourLbl.setText("EWS Colour Code");

        ewsCanvas.setSize(new java.awt.Dimension(100, 50));

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(ewsCanvas, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(scoreLbl)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(scoreLblStat))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(sensersUpdateLbl)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(sensersLblStat))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(ewsUpdateLbl)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(ewsLblStat))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(ewsColourLbl)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(scoreLblStat)
                    .addComponent(scoreLbl))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(ewsUpdateLbl)
                    .addComponent(ewsLblStat))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(sensersUpdateLbl)
                    .addComponent(sensersLblStat))
                .addGap(37, 37, 37)
                .addComponent(ewsColourLbl)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(ewsCanvas, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        jTextPath.setEditable(false);

        startBtn.setText("Start");
        startBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                startBtnActionPerformed(evt);
            }
        });

        stopBtn.setText("Stop");
        stopBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
            	try{
            		stopBtnActionPerformed(evt);
            	}catch(InterruptedException ex){
        		
            	}
            }
        });
        file.setText("File");
        
        jTFFile.setEditable(false);
        jTFFile.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        jTFFile.setText("No File Set!");
        
        fileOpen.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_O, java.awt.event.InputEvent.ALT_MASK));
        fileOpen.setText("Open");
        fileOpen.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                fileOpenActionPerformed(evt);
            }
        });
        file.add(fileOpen);

        fileQuit.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_Q, java.awt.event.InputEvent.ALT_MASK));
        fileQuit.setText("Quit Monitor");
        fileQuit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                fileQuitActionPerformed(evt);
            }
        });
        file.add(fileQuit);

        manuBar.add(file);

        options.setText("Options");

        optSI.setText("Set Sample Interval");

        si0.setText("Default");
        optSI.add(si0);
        si0.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                si0ActionPerformed(evt);
            }
        });

        si5.setText("5 Seconds");
        optSI.add(si5);
        si5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
            	si5ActionPerformed(evt);
            }
        });

        si10.setText("10 Seconds");
        optSI.add(si10);
        si10.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
            	si10ActionPerformed(evt);
            }
        });
        si15.setText("15 Seconds");
        optSI.add(si15);
        si15.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
            	si15ActionPerformed(evt);
            }
        });
        options.add(optSI);

        optEI.setText("Set Evaluation Interval");

        ei0.setText("Default");
        optEI.add(ei0);
        ei0.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
            	ei0ActionPerformed(evt);
            }
        });

        ei5.setText("5 Minuts");
        optEI.add(ei5);
        ei5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
            	ei5ActionPerformed(evt);
            }
        });
        ei10.setText("10 Minuts");
        optEI.add(ei10);
        ei10.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
            	ei10ActionPerformed(evt);
            }
        });
        ei30.setText("15 Minuts");
        optEI.add(ei30);
        ei30.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
            	ei30ActionPerformed(evt);
            }
        });
        options.add(optEI);

        manuBar.add(options);

        view.setText("View");

        showAnalyser.setSelected(false);
        showAnalyser.setText("Show Analyser");
        showAnalyser.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
            	showAnalyserActionPerformed(evt);
            }
        });
        
        view.add(showAnalyser);

        manuBar.add(view);

        setJMenuBar(manuBar);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollFile, javax.swing.GroupLayout.DEFAULT_SIZE, 609, Short.MAX_VALUE)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jpanStats, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(startBtn)
                    .addComponent(stopBtn))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))

            .addComponent(jTFFile, javax.swing.GroupLayout.Alignment.TRAILING)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jpanStats, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(startBtn)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(stopBtn)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollFile, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(8, 8, 8)
                .addComponent(jTFFile, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        pack();
    }// </editor-fold>                        

    
    private void msgbox(String s) {
    	//easy acsess to the message dialog widget
        JOptionPane.showMessageDialog(null, s, "INFO", JOptionPane.WARNING_MESSAGE);
    } 
    
    //when open is clocked in the menu show the file dialog and set the file
    //does not check type
    private void fileOpenActionPerformed(java.awt.event.ActionEvent evt) {                                         
        int returnVal = monFileOpen.showOpenDialog(this);
        if (returnVal == JFileChooser.APPROVE_OPTION) {
            paitentFile = monFileOpen.getSelectedFile();
            jTFFile.setText(paitentFile.getAbsolutePath());
        }
    }                                        

    private void fileQuitActionPerformed(java.awt.event.ActionEvent evt) {                                         
        System.exit(0);
    }                                        

	private void startBtnActionPerformed(java.awt.event.ActionEvent evt) {
		
		// start to run the simulation
		if (RUNNING == 0) { // only run if running has not been changed
			if (paitentFile != null) {  //only run if theres a file
				RUNNING = 1; //prevent the thread being cauled again
				runner = new CW1MonWorker(fileTextArea, rrLblStat, o2LblStat,
						tempLblStat, bpLblStat, hrLblStat, SI, paitentFile,
						ewsCanvas, scoreLblStat);  //all thease gui elaments will be updated by the worker
				runnerT = new Thread(runner);
				runnerT.start();
			}else{
				msgbox("File not Loaded.");
			}
		}
	}                                    

    private void stopBtnActionPerformed(java.awt.event.ActionEvent evt) throws InterruptedException{
    	//stop the runnerT thread
    		if ( runnerT != null){
    			runner.stop();
    			runnerT.join();
    			RUNNING = 0;
    		}
    }
    
    private void showAnalyserActionPerformed (java.awt.event.ActionEvent evt){
    	java.awt.EventQueue.invokeLater(new Runnable() {
    		//show the analyser window if toggeld
            public void run() {
                new Analyser().setVisible(true);
            }
        });
    }
    
    //SI and EI intervals that can be set in the menu
    private void si0ActionPerformed(java.awt.event.ActionEvent evt) {                                    
    	SI = 1000;
        sensersLblStat.setText("1 Second");
    }
    private void si5ActionPerformed(java.awt.event.ActionEvent evt) {                                    
    	SI = 5000;
    	sensersLblStat.setText("5 Second");
    }
    private void si10ActionPerformed(java.awt.event.ActionEvent evt) {                                    
    	SI = 10000;
    	sensersLblStat.setText("10 Second");
    }
    private void si15ActionPerformed(java.awt.event.ActionEvent evt) {                                    
    	SI = 15000;
    	sensersLblStat.setText("15 Second");
    } 
    private void ei0ActionPerformed(java.awt.event.ActionEvent evt) {                                    
    	EI = 60000;
    	ewsLblStat.setText("1 Minut");
    }
    private void ei5ActionPerformed(java.awt.event.ActionEvent evt) {                                    
    	EI = 300000;
    	ewsLblStat.setText("5 Minuts");
    }
    private void ei10ActionPerformed(java.awt.event.ActionEvent evt) {                                    
    	EI = 600000;
    	ewsLblStat.setText("10 Minuts");
    }
    private void ei30ActionPerformed(java.awt.event.ActionEvent evt) {                                    
    	EI = 1800000;
    	ewsLblStat.setText("15 Minuts");
    } 

    
    public static void main(String args[]) {

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Monitor().setVisible(true);
            }
        });
    }

                  
}
