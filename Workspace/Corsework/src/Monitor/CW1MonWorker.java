package Monitor;

import java.awt.Canvas;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.swing.*;
import patientSim.*;
import systemsAnalyser.Analyser;
import ewsAlgorithm.*;
public class CW1MonWorker implements Runnable{
	
	// f[0] is Hart Rate
	// f[1] is BP
	// f[2] is Temp
	// f[3] is RespRate
	// f[4] is O2
	
	private int STOP = 0;
	private JTextArea jta;
	private JLabel rrLblStat;
	private JLabel o2LblStat;
	private JLabel tempLblStat;
	private JLabel bpLblStat;
	private JLabel hrLblStat;
	private int si;
	private File paitentFile;
	private Canvas ewsCanvas;
	private JLabel scoreLblStat;
	int AmberCount =0;
	Boolean RED = false;
	//take in all that is to be updated from the gui
	CW1MonWorker(JTextArea jta, JLabel rrLblStat, JLabel o2LblStat,
			JLabel tempLblStat, JLabel bpLblStat, JLabel hrLblStat, int si,
			File paitentFile, Canvas ewsCanvas, JLabel scoreLblStat) {
		this.jta = jta;
		this.rrLblStat = rrLblStat;
		this.o2LblStat = o2LblStat;
		this.tempLblStat = tempLblStat;
		this.bpLblStat = bpLblStat;
		this.hrLblStat = hrLblStat;
		this.si = si;
		this.paitentFile = paitentFile;
		this.ewsCanvas = ewsCanvas;
		this.scoreLblStat = scoreLblStat;
	}
	public void stop(){
		STOP = 1;
	}
	
	@Override
	public void run() {
		while( STOP != 1) {
			doWork(si);
		}
	}
	//call the cim, parse the file, calculate the stats
	private void doWork(int si) {
		PatientSim sim = new PatientSim();
		List<float[]> patientStats = new ArrayList<float[]>();
		patientStats = sim.getStats(paitentFile.getAbsolutePath());
		for (float[] f : sim.ParseList(patientStats)) {
			try {
				if (STOP != 1) {
					rrLblStat.setText(Float.toString(f[3]));
					o2LblStat.setText(Float.toString(f[4]));
					tempLblStat.setText(Float.toString(f[2]));
					bpLblStat.setText(Float.toString(f[1]));
					hrLblStat.setText(Float.toString(f[0]));
					jta.append(calcStats(f));
					Thread.sleep(si);
				}

			} catch (InterruptedException ex) {
				Thread.currentThread().interrupt();
			}
		}
	}
	
	private String calcStats(float[] f){
		NEWS ews = new NEWS();
		PhysioParams params = new PhysioParams();
		params.setHartRate((int) f[0]);
		params.setBloodPresh((int) f[1]);
		params.setTemp((double) f[2]);
		params.setRespRate((int) f[3]);
		params.setO2Sat((int) f[4]);
		//once the stats have been cast right, pass them to the calculator and retunr the score.
		//anythign that neads the score is pased it, and othere stats
		int score = ews.Calculate(params);
		setEwsCanvas(score);
		Analyser.setScoreEWS(score);
		Analyser.setStats(f);
		System.out.println(Arrays.toString(f)+ "Score:"+ score);  //console debug
		String out = Arrays.toString(f) + " Score: " + score +"\n";
		return out;
	}
	
    private void setEwsCanvas(int score){
    	//update the colour on the screen
    	if (score <=1){
    		ewsCanvas.setBackground(new java.awt.Color(124,252,0)); //Grean
    		scoreLblStat.setText(Integer.toString(score));
    	}else if(score >=5){
    		ewsCanvas.setBackground(new java.awt.Color(204,17,0)); //Red
    		scoreLblStat.setText(Integer.toString(score));
    		RED = true;
    	}else{
    		if ( RED != true){//prevent the red being unset if the score is still under 5
    			ewsCanvas.setBackground(new java.awt.Color(255, 126, 0)); //Amber
    			scoreLblStat.setText(Integer.toString(score));
    			AmberCount = AmberCount + 1;
    			System.out.println(AmberCount);
    			if (AmberCount == 5){ //if 5 ambers have ocurd set the alarm for 5 seconds to change to red
    				new EWSAlarm(5 ,ewsCanvas);
    				AmberCount = 0;
    				RED = true;
    			}
    		}
    		
    	}

    }
	

	
}
