package Monitor;

import java.awt.Canvas;
import java.util.*;

public class EWSAlarm {
	//this is used for the colour to be changed to red if its was amber for more than 5 seconds
	
    Timer timer;
    private Canvas ewsCanvas;

    public EWSAlarm(int seconds, Canvas ewsCanvas) {
        timer = new Timer();
        timer.schedule(new elevateColour(), seconds*1000);
        this.ewsCanvas = ewsCanvas;
	}
    class elevateColour extends TimerTask {
        public void run() {
        	ewsCanvas.setBackground(new java.awt.Color(204,17,0)); //Red
        	System.out.println("Alarm: Colour Elevated to RED!");
            timer.cancel(); //terminate the timer thread
        }
    }
}
