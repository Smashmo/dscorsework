package systemsAnalyser;

import Monitor.EWSAlarm;

public class Analyser extends javax.swing.JFrame {
	//as there is no proper instence of this class out side it being showen
	//all vars that are modified are static as the metherds are cauld directly
    private static java.awt.Canvas canvasColour;
    private javax.swing.JLabel ewsSALbl;
    private static javax.swing.JLabel ewsSALblStat;
    private static javax.swing.JTextField jtfDiag;
	static int AmberCount =0;
	static Boolean RED = false;

	//take in the score from the worker and display it on screen
	public static void setScoreEWS(int scoreEWS) {
		ewsSALblStat.setText(Integer.toString(scoreEWS));
		setEwsCanvas(scoreEWS);
	}
	
	//acses to copys of the stats from the worker so a diag can be sugested
	public static void setStats (float [] statsF){
		CalcDiag((int) statsF[0], (int) statsF[1], (double) statsF[2], (int) statsF[3], (int) statsF[4]);
	}
	
	private static void setEwsCanvas(int score){
    	//update the colour on the screen
    	if (score <=1){
    		canvasColour.setBackground(new java.awt.Color(124,252,0)); //Grean
    	}else if(score >=5){
    		canvasColour.setBackground(new java.awt.Color(204,17,0)); //Red
    		RED = true;
    	}else{
    		if ( RED != true){ //prevent the red being unset if the score is still under 5
    			canvasColour.setBackground(new java.awt.Color(255, 126, 0)); //Amber
    			AmberCount = AmberCount + 1;
    			System.out.println(AmberCount);
    			if (AmberCount == 5){  //if 5 ambers have ocurd set the alarm for 5 seconds to change to red
    				new EWSAlarm(5 ,canvasColour);
    				AmberCount = 0;
    				RED = true;
    			}
    		}
    		
    	}
	}
	//sugest a posibal diag beased on the stats
	private static void CalcDiag(int hartRate, int bloodPresh, double temp, int respRate, int o2Stat ){
		if ((hartRate >=80) && (bloodPresh <=120)){
			jtfDiag.setText("Gastrointestinal (GI) Bleeding");
		}else if((hartRate<=65)&&(bloodPresh>=220)&&(o2Stat<=97)){
			jtfDiag.setText("Stroke");
		}else if((hartRate>=110)&&(bloodPresh>=150)&&(o2Stat<=97)){
			jtfDiag.setText("Myocardial Infarction");
		}else{
			jtfDiag.setText("Diagnosis OK");
		}
	}
	
    public Analyser() {
        initComponents();  //gui wigets instansated here
    }
    
    private void initComponents() {

        jtfDiag = new javax.swing.JTextField();
        ewsSALbl = new javax.swing.JLabel();
        canvasColour = new java.awt.Canvas();
        ewsSALblStat = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jtfDiag.setEditable(false);
        jtfDiag.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        jtfDiag.setText("No Sugested Diagnosis ");

        ewsSALbl.setText("EWS Score");

        ewsSALblStat.setText("NULL");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(33, 33, 33)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(ewsSALbl)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(ewsSALblStat))
                    .addComponent(canvasColour, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jtfDiag))
                .addContainerGap(32, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(24, 24, 24)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(ewsSALbl)
                    .addComponent(ewsSALblStat))
                .addGap(29, 29, 29)
                .addComponent(canvasColour, javax.swing.GroupLayout.PREFERRED_SIZE, 62, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jtfDiag, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(45, Short.MAX_VALUE))
        );

        pack();
    }
  

}
