package patientSim;


import java.util.*;


public class PatientSim {
	//give acsess to the file now stored in memory after it has been dealt with by the file loader
	
	public List<float[]> getStats(String file){
		List<float[]> stats;
		stats = FileLoader.Read(file);
		return stats;
	}
		
	public List<float[]> ParseList(List<float[]> patientStats){
		List<float[]> stat = new ArrayList<float[]>();	
		for ( int i = 0; i < patientStats.size(); i++){
			stat.add(patientStats.get(i));			
		}
		return(stat);
		
	}
	
}
