package SymptomsAnlyser;

import EWS.EWSBean;

public class Analyser {
		private static int timer = 0;
		private static Boolean RED = false;
		 
	public static int AlarmColour(EWSBean bean) {
			int alarmVal = 1;
    	int score = bean.getScore();
    	//update the colour on the screen
    	
    	if (score <=1){
    		
    		alarmVal = 0;
    		
    	}else if(score >=5){
    		//Red
    		RED = true;
    		alarmVal = 5;
    		bean.setDiag(setStats(bean));
    	}else{
    		if ( RED != true){//prevent the red being unset if the score is still under 5
    			//Amber
    			//start timer if expores force read
    			alarmVal = 2;
    			bean.setDiag(setStats(bean));
    			
    			try {
    				if (alarmVal != 0){
    				Thread.sleep(1000);
    				timer += 1;
    				if (timer == 5){
        				alarmVal = 5;
        				bean.setDiag(setStats(bean));
        				RED = true;
    				}
    			}
    				
    			} catch (InterruptedException e) {
    				// TODO Auto-generated catch block

    			}
    			
    		}
    	}
		return alarmVal;
    		

	}

	//acses to copys of the stats from the worker so a diag can be sugested
	private static String setStats (EWSBean bean){
		return CalcDiag(bean.getHartRate(),
				bean.getBloodPresh(), bean.getTemp(), 
				bean.getRespRate(), bean.getO2Sat());
	}
	
	//sugest a posibal diag beased on the stats
	private static String CalcDiag(int hartRate, int bloodPresh, double temp, int respRate, int o2Stat ){
		String diag = "";
		if ((hartRate >=80) && (bloodPresh <=120)){
			diag = "Gastrointestinal (GI) Bleeding";
		}else if((hartRate<=65)&&(bloodPresh>=220)&&(o2Stat<=97)){
			diag = "Stroke";
		}else if((hartRate>=110)&&(bloodPresh>=150)&&(o2Stat<=97)){
			diag = "Myocardial Infarction";
		}else{
			diag = "Diagnosis OK";
		}
		return diag;
	}


}
