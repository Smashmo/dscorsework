package Monitor;

import java.util.Date;

import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;

import EWS.EWSBean;
import EWS.NEWS;
import SymptomsAnlyser.Analyser;


@WebService(serviceName = "core")
public class core{


	
	//methord to into EWS algorithom to calculate the score
	
	// methrod to work out fron the score if it shoudl rase the alamr
	
	//methord that returns the stats, and score for a given evaluation interval
	
	//as well as sending the stats send back the color and only if neaded fill
	// the simptom if requierd ( dosent nead to happen if they are green

	
	//if time send evrythign to postgresql
	@WebMethod(operationName = "pole")
	public String pole (){
		return "MON:OK";
	}
	@WebMethod(operationName = "MonInit")
	public EWSBean MonInit(@WebParam(name = "ewsBean")EWSBean ewsBean ,@WebParam(name = "EI") int EI){
		EWSBean finalBean = new EWSBean();
		
		finalBean = calcStats(ewsBean);
		finalBean.setTimestamp(getDateString());
		finalBean.setColour(Analyser.AlarmColour(finalBean));
		try {
			Thread.sleep(EI);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return finalBean;
	}
	
	
	private EWSBean calcStats(EWSBean statsBean){
		NEWS ews = new NEWS();
		//once the stats have been cast right, pass them to the calculator and retunr the score.
		//anythign that neads the score is pased it, and othere stats
		statsBean.setScore(ews.Calculate(statsBean));
		return statsBean;
	}
	

	
	private String getDateString() {
		Date now = new Date();
		return now.toString();
	}
}