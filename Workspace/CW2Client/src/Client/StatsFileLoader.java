package Client;

import java.io.*;
import java.util.*;

public class StatsFileLoader {

	public static List<float[]> Read(String file){
		List<float[]>patientStats = new ArrayList<float[]>(); 
		try(BufferedReader reader = new BufferedReader(new FileReader(file))){
			//open file pasted to function
			String line = null;
			List<String[]> lines = new ArrayList<String[]>();
			//list need to be of String array, as split returns this type
			//this will give a list of string arrays
			
			while((line = reader.readLine()) != null){
					lines.add(line.split("\t"));
			}
			
			for ( String[] s : lines){
				//add each string array to list
				patientStats.add(Cast_StringArray(Arrays.toString(s)));
			}
			patientStats.remove(0); //Strip of the heading for the file
		}
		catch (IOException x){
			System.err.format("IOException: %s%n", x);
		}
		return(patientStats);
	}
	
	static float[] Cast_StringArray(String stringArray){
		 
		String unnest = stringArray.replaceAll("\\[*", "").replaceAll("\\]", ", "); // undoo any extra list formating
		String[] items = unnest.split(", "); //cleanly put it back in a list undoing the reviouse comma seperation
		
		float[] float_array = new float[items.length]; //return def 
		
			for ( int i =0; i < items.length; i++){ //try to cast each item as a foat and put in list
				try{
					float_array[i] = Float.parseFloat(items[i]);
				}
				catch (NumberFormatException ex){
					System.out.println(ex); //any strings or garbage will be cought
				}
			}
		
		return float_array; //return the list
	}
}
