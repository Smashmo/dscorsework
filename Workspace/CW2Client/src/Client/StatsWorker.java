package Client;
/*
import java.io.*;
import java.util.*;
import org.eclipse.swt.widgets.Label;
import simulator.EwsBean;

public class StatsWorker implements Runnable {

    private File file;
    private int si;
    private Label lblRR, lblO2, lblTmp, lblBP, lblHR;
            
    
    StatsWorker(File patentFile, int SI, Label BP, Label HR, Label O2, Label RR, Label Tmp){
        this.file = patentFile;
        this.si = SI;
        this.lblBP = BP;
        this.lblHR = HR;
        this.lblO2 = O2;
        this.lblRR = RR;
        this.lblTmp = Tmp;
        
    }
    
    public void stop(){
        
    }
        
    public void run(){
        doWork(this.file, this.si);
    }
    
    
    public void doWork(File paitentFile, int SI){

            List<float[]> patientStats = getStatsFile(paitentFile.getAbsolutePath());

            // get the file in the List<float[]> and pass to the webservice
            //this below must be done in the web service
            simulator.EwsBean ewsBean;
            for (float[] f : ParseList(patientStats)) {
                    //System.out.println(Arrays.toString(f));

                    // turn into a bean and send to web servers
                    simulator.StatBean params = setStatsBean(f);
                    ewsBean = getStats(params, SI);
                    lblBP.setText(Integer.toString(ewsBean.getBloodPresh()));
                    //send params to the webservice

            }
    }

    private simulator.StatBean setStatsBean(float[] f){
            simulator.StatBean statBean = new simulator.StatBean();
            statBean.setHartRate((int) f[0]);
            statBean.setBloodPresh((int) f[1]);
            statBean.setTemp((double) f[2]);
            statBean.setRespRate((int) f[3]);
            statBean.setO2Sat((int) f[4]);

            return statBean;
    }


    private List<float[]> getStatsFile(String file){
            List<float[]> stats;
            stats = StatsFileLoader.Read(file);
            return stats;
    }


    //this is just to test that the UI its togethere
    private List<float[]> ParseList(List<float[]> patientStats){
            List<float[]> stat = new ArrayList<float[]>();	
            for ( int i = 0; i < patientStats.size(); i++){
                    stat.add(patientStats.get(i));			
            }
            return(stat);

    }

    private static EwsBean getStats(simulator.StatBean stBean, int si) {
        simulator.Main service = new simulator.Main();
        simulator.Core port = service.getCorePort();
        return port.getStats(stBean, si);
    }


}
*/