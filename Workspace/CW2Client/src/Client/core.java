 package Client;

import java.io.File;
import java.util.Arrays;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;

import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Canvas;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;

public class core {
        Display display;

	protected Shell shlCwClient;
	private Text coreFilePath;
	private javax.swing.JFileChooser monFileOpen;
        JFileChooser fc;  //diclare the file open dialog
        File paitentFile; //file to be pased and displayed
	Thread statsThread;
        /**
	 * Launch the application.
	 * @param args
	 */
	public static void main(String[] args) {
		try {
			core window = new core();
			window.open();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Open the window.
	 */
	public void open() {
		display = Display.getDefault();
		createContents();
		shlCwClient.open();
		shlCwClient.layout();
		while (!shlCwClient.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
	}

    private void msgbox(String s) {
    	//easy acsess to the message dialog widget
        JOptionPane.showMessageDialog(null, s, "INFO", JOptionPane.WARNING_MESSAGE);
    } 
	
	/**
	 * Create contents of the window.
	 * @wbp.parser.entryPoint
	 */
	protected void createContents() {
		shlCwClient = new Shell();
		shlCwClient.setSize(500, 600);
		shlCwClient.setText("CW2 Client");
		
		Menu coreMenu = new Menu(shlCwClient, SWT.BAR);
		shlCwClient.setMenuBar(coreMenu);
		
		MenuItem coreMenuFile = new MenuItem(coreMenu, SWT.CASCADE);
		coreMenuFile.setText("File");
		
		Menu menu = new Menu(coreMenuFile);
		coreMenuFile.setMenu(menu);
		
		monFileOpen = new javax.swing.JFileChooser();
		MenuItem coreMFileOpen = new MenuItem(menu, SWT.NONE);
		coreMFileOpen.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				
		        int returnVal = monFileOpen.showOpenDialog(monFileOpen);
		        if (returnVal == JFileChooser.APPROVE_OPTION) {
		            paitentFile = monFileOpen.getSelectedFile();
		            coreFilePath.setText(paitentFile.getAbsolutePath());
		        }
			}
		});
		coreMFileOpen.setText("Open");
		
		MenuItem coreMFileQuit = new MenuItem(menu, SWT.NONE);
		coreMFileQuit.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				System.exit(0);
			}
		});
		coreMFileQuit.setText("Quit Client");
		
		MenuItem coreMenuOpt = new MenuItem(coreMenu, SWT.CASCADE);
		coreMenuOpt.setText("Options");
		
		Menu menu_1 = new Menu(coreMenuOpt);
		coreMenuOpt.setMenu(menu_1);
		
		MenuItem coreMOptSI = new MenuItem(menu_1, SWT.CASCADE);
		coreMOptSI.setText("Set Sample Interval");
		
		Menu menu_2 = new Menu(coreMOptSI);
		coreMOptSI.setMenu(menu_2);
		
		MenuItem menuSIDefault = new MenuItem(menu_2, SWT.NONE);
		menuSIDefault.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
			}
		});
		menuSIDefault.setText("Default");
		
		MenuItem menuSI5 = new MenuItem(menu_2, SWT.NONE);
		menuSI5.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
			}
		});
		menuSI5.setText("5 Seconds");
		
		MenuItem menuSI10 = new MenuItem(menu_2, SWT.NONE);
		menuSI10.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
			}
		});
		menuSI10.setText("10 Seconds");
		
		MenuItem coreMOptEI = new MenuItem(menu_1, SWT.CASCADE);
		coreMOptEI.setText("Set Evaluation Interval");
		
		Menu menu_3 = new Menu(coreMOptEI);
		coreMOptEI.setMenu(menu_3);
		
		MenuItem menuEIDefault = new MenuItem(menu_3, SWT.NONE);
		menuEIDefault.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
			}
		});
		menuEIDefault.setText("Default");
		
		MenuItem menuEI5 = new MenuItem(menu_3, SWT.NONE);
		menuEI5.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
			}
		});
		menuEI5.setText("5 Seconds");
		
		MenuItem menuEI10 = new MenuItem(menu_3, SWT.NONE);
		menuEI10.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
			}
		});
		menuEI10.setText("10 Seconds");
		
		MenuItem coreMOptPole = new MenuItem(menu_1, SWT.CASCADE);
		coreMOptPole.setText("Set Service Alive Pole");
		
		Menu menu_5 = new Menu(coreMOptPole);
		coreMOptPole.setMenu(menu_5);
		
		MenuItem menuPoleDefault = new MenuItem(menu_5, SWT.NONE);
		menuPoleDefault.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
			}
		});
		menuPoleDefault.setText("Default");
		
		MenuItem menuPole2 = new MenuItem(menu_5, SWT.NONE);
		menuPole2.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
			}
		});
		menuPole2.setText("2 Minutes");
		
		MenuItem menuPole10 = new MenuItem(menu_5, SWT.NONE);
		menuPole10.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
			}
		});
		menuPole10.setText("10 Minutes");
		
		MenuItem coreMenuView = new MenuItem(coreMenu, SWT.CASCADE);
		coreMenuView.setText("View");
		
		Menu menu_4 = new Menu(coreMenuView);
		coreMenuView.setMenu(menu_4);
		
		MenuItem coreMViewAnalyser = new MenuItem(menu_4, SWT.NONE);
		coreMViewAnalyser.setText("Show Analyser");
		
		Group grpSensors = new Group(shlCwClient, SWT.NONE);
		grpSensors.setText("Sensors");
		grpSensors.setBounds(10, 10, 229, 155);
		
		Label lblResperationRate = new Label(grpSensors, SWT.NONE);
		lblResperationRate.setBounds(10, 29, 110, 17);
		lblResperationRate.setText("Resperation Rate");
		
		Label lblOxygenLevel = new Label(grpSensors, SWT.NONE);
		lblOxygenLevel.setText("Oxygen Level");
		lblOxygenLevel.setBounds(10, 52, 110, 17);
		
		Label lblTemprature = new Label(grpSensors, SWT.NONE);
		lblTemprature.setText("Temprature");
		lblTemprature.setBounds(10, 76, 110, 17);
		
		Label lblBlodPreshure = new Label(grpSensors, SWT.NONE);
		lblBlodPreshure.setText("Blood Pressure");
		lblBlodPreshure.setBounds(10, 99, 110, 17);
		
		Label lblHartRate = new Label(grpSensors, SWT.NONE);
		lblHartRate.setText("Heart Rate");
		lblHartRate.setBounds(10, 122, 110, 17);
		
		final Label lblRR = new Label(grpSensors, SWT.NONE);
		lblRR.setBounds(157, 29, 62, 17);
		lblRR.setText("NULL");
		
		final Label lblO2 = new Label(grpSensors, SWT.NONE);
		lblO2.setText("NULL");
		lblO2.setBounds(157, 52, 62, 17);
		
		final Label lblTmp = new Label(grpSensors, SWT.NONE);
		lblTmp.setText("NULL");
		lblTmp.setBounds(157, 76, 62, 17);
		
		final Label lblBP = new Label(grpSensors, SWT.NONE);
		lblBP.setText("NULL");
		lblBP.setBounds(157, 99, 62, 17);
		
		final Label lblHR = new Label(grpSensors, SWT.NONE);
		lblHR.setText("NULL");
		lblHR.setBounds(157, 122, 62, 17);
		
		Group grpMonitor = new Group(shlCwClient, SWT.NONE);
		grpMonitor.setText("Monitor");
		grpMonitor.setBounds(10, 171, 229, 201);
		
		Label lblCurentEwsScore = new Label(grpMonitor, SWT.NONE);
		lblCurentEwsScore.setText("Curent EWS Score");
		lblCurentEwsScore.setBounds(10, 29, 110, 17);
		
		Label lblSampleInterval = new Label(grpMonitor, SWT.NONE);
		lblSampleInterval.setText("Sample Interval");
		lblSampleInterval.setBounds(10, 60, 110, 17);
		
		Label lblEvaluationInterval = new Label(grpMonitor, SWT.NONE);
		lblEvaluationInterval.setText("Evaluation Interval");
		lblEvaluationInterval.setBounds(10, 83, 110, 17);
		
		Label lblEwsColourCode = new Label(grpMonitor, SWT.NONE);
		lblEwsColourCode.setText("EWS Colour Code");
		lblEwsColourCode.setBounds(10, 106, 110, 17);
		
		Canvas EWSCanvas = new Canvas(grpMonitor, SWT.NONE);
		EWSCanvas.setBounds(10, 129, 209, 64);
		
		Label lblEWSScore = new Label(grpMonitor, SWT.NONE);
		lblEWSScore.setText("NULL");
		lblEWSScore.setBounds(157, 29, 62, 17);
		
		Label lblSI = new Label(grpMonitor, SWT.NONE);
		lblSI.setText("NULL");
		lblSI.setBounds(157, 60, 62, 17);
		
		Label lblEI = new Label(grpMonitor, SWT.NONE);
		lblEI.setText("NULL");
		lblEI.setBounds(157, 83, 62, 17);
		
		Label lblColour = new Label(grpMonitor, SWT.NONE);
		lblColour.setText("NULL");
		lblColour.setBounds(157, 106, 62, 17);
		
		Group grpServiceStatus = new Group(shlCwClient, SWT.NONE);
		grpServiceStatus.setText("Service Status");
		grpServiceStatus.setBounds(245, 10, 241, 155);
		
		Label lblSimulatorService = new Label(grpServiceStatus, SWT.NONE);
		lblSimulatorService.setText("Simulator Service");
		lblSimulatorService.setBounds(10, 28, 110, 17);
		
		Label lblMonitorService = new Label(grpServiceStatus, SWT.NONE);
		lblMonitorService.setText("Monitor Service");
		lblMonitorService.setBounds(10, 63, 110, 17);
		
		Label lblSimS = new Label(grpServiceStatus, SWT.NONE);
		lblSimS.setText("NULL");
		lblSimS.setBounds(169, 28, 62, 17);
		
		Label lblMonS = new Label(grpServiceStatus, SWT.NONE);
		lblMonS.setText("NULL");
		lblMonS.setBounds(169, 63, 62, 17);
		
		Label lblPolingInterval = new Label(grpServiceStatus, SWT.NONE);
		lblPolingInterval.setBounds(10, 100, 110, 17);
		lblPolingInterval.setText("Poling Interval");
		
		Label lblPI = new Label(grpServiceStatus, SWT.NONE);
		lblPI.setText("NULL");
		lblPI.setBounds(169, 100, 62, 17);
		
		final StyledText coreStatsDisplay = new StyledText(shlCwClient, SWT.BORDER | SWT.V_SCROLL);
		coreStatsDisplay.setEditable(false);
		coreStatsDisplay.setDragDetect(false);
		coreStatsDisplay.setDoubleClickEnabled(false);
		coreStatsDisplay.setWordWrap(true);
		coreStatsDisplay.setBounds(10, 378, 476, 132);
		
		coreFilePath = new Text(shlCwClient, SWT.BORDER);
		coreFilePath.setText("No File Set!");
		coreFilePath.setDragDetect(false);
		coreFilePath.setDoubleClickEnabled(false);
		coreFilePath.setEditable(false);
		coreFilePath.setBounds(10, 516, 476, 27);
		
		Button btnStart = new Button(shlCwClient, SWT.NONE);
		btnStart.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				if (paitentFile != null) {
					
					
					//StatsWorker w = new StatsWorker(paitentFile,0,
                                        //        lblRR,lblO2,lblTmp,lblBP,lblHR);
                                        //statsThread = new Thread(w);
                                        //statsThread.start();
					
				}else{
					msgbox("NO FILE SET!");
				}
			}
		});
		btnStart.setBounds(245, 184, 82, 27);
		btnStart.setText("Start");
		
		Button btnStop = new Button(shlCwClient, SWT.NONE);
		btnStop.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				
			}
		});
		btnStop.setText("Stop");
		btnStop.setBounds(245, 217, 82, 27);
	
	}
}
