package EWS;

public class NEWS {
	//calculate the score with a nested if statment.
	//the score is added to a 5 element array
	//for each int in the array this will be the score.
	//and this is the returnd.
	
	public int Calculate(PhysioParams patient){
		int[] score;
		score = new int[5];

		int NEWS_score = 0;
		
		if ((patient.getRespRate() >=12) && (patient.getRespRate() <=20)){
			score[0] = 0;
		}else if ((patient.getRespRate() >= 9) && (patient.getRespRate() <= 11)){
			score[0] = 1;
		}else if ((patient.getRespRate() >= 21) && (patient.getRespRate() <= 24)){
			score[0] = 2;
		}else if ((patient.getRespRate() <= 8) || (patient.getRespRate() >= 25)){
			score[0] = 3;
		}
		
		if (patient.getO2Sat() >=96){
			score[1] = 0;
		}else if ((patient.getO2Sat() >= 94) && (patient.getO2Sat() <= 95)){
			score[1] = 1;
		}else if ((patient.getO2Sat() >= 92) && (patient.getO2Sat() <= 93)){
			score[1] = 2;
		}else if (patient.getO2Sat() <= 91){
			score[1] = 3;
		}
		
		if ((patient.getTemp() >=36.1) && (patient.getTemp() <=38.0)){
			score[2] = 0;
		}else if ((patient.getTemp() >= 38.1) && (patient.getTemp() <= 39.0) || 
				(patient.getTemp() >= 35.1) && (patient.getTemp() <= 36.0)){
			score[2] = 1;
		}else if (patient.getTemp() >= 39.1){
			score[2] = 2;
		}else if (patient.getTemp() <= 35.0){
			score[2] = 3;
		}
		
		if ((patient.getBloodPresh() >=111) && (patient.getBloodPresh() <=219)){
			score[3] = 0;
		}else if ((patient.getBloodPresh() >= 101) && (patient.getBloodPresh() <= 111)){
			score[3] = 1;
		}else if ((patient.getBloodPresh() >= 91) && (patient.getBloodPresh() <= 100)){
			score[3] = 2;
		}else if ((patient.getBloodPresh() <= 90) || (patient.getBloodPresh() >= 220)){
			score[3] = 3;
		}

		if ((patient.getHartRate() >=51) && (patient.getHartRate() <=90)){
			score[4] = 0;
		}else if ((patient.getHartRate() >= 91) && (patient.getHartRate() <= 110) ||
				(patient.getHartRate() >= 41) && (patient.getHartRate() <= 50)){
			score[4] = 1;
		}else if ((patient.getHartRate() >= 111) && (patient.getHartRate() <= 130)){
			score[4] = 2;
		}else if ((patient.getHartRate() <= 40) || (patient.getHartRate() >= 131)){
			score[4] = 3;
		}
		
		for ( int i = 0; i < 4; i++){
			NEWS_score += score[i];
		}
		
		return NEWS_score;
	}
}