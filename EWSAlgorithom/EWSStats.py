class EWSStats(object):
    def __init__(self, RespRate, O2Stat, Temp, BloodPresh, HartRate):
        self.resp_rate = RespRate
        self.o2_stat = O2Stat
        self.temprature = Temp
        self.blood_presh = BloodPresh
        self.hart_rate = HartRate
