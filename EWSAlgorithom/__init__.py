class EWSAlgorithom(object):
    def __init__(self):
        self.score = []
        self.stat = 0
        self.EWS_score = 0

    def Calculate(self, patient):
        if patient.resp_rate >=12 and patient.resp_rate <=20:
            self.score.append(0)
        elif patient.resp_rate >= 9 and patient.resp_rate <= 11:
            self.score.append(1)
        elif patient.resp_rate >= 21 and patient.resp_rate <= 24:
            self.score.append(2)
        elif patient.resp_rate <= 8 or patient.resp_rate >= 25:
            self.score.append(3)

        if patient.o2_stat >=96:
            self.score.append(0)
        elif patient.o2_stat >= 94 and patient.o2_stat <= 95:
            self.score.append(1)
        elif patient.o2_stat >= 92 and patient.o2_stat <= 93:
            self.score.append(2)
        elif patient.o2_stat <= 91:
            self.score.append(3)


        if patient.temprature >=36.1 and patient.temprature <=38.0:
            self.score.append(0)
        elif patient.temprature >= 38.1 and patient.temprature <= 39.0 or patient.temprature >= 35.1 and patient.temprature <= 36.0:
            self.score.append(1)
        elif patient.temprature >= 39.1:
            self.score.append(2)
        elif patient.temprature <= 35.0:
            self.score.append(3)


        if patient.blood_presh >=111 and patient.blood_presh <=219:
            self.score.append(0)
        elif patient.blood_presh >= 101 and patient.blood_presh <= 111:
            self.score.append(1)
        elif patient.blood_presh >= 91 and patient.blood_presh <= 100:
            self.score.append(2)
        elif patient.blood_presh <= 90 or patient.blood_presh >= 220:
            self.score.append(3)


        if patient.hart_rate >=51 and patient.hart_rate <=90:
            self.score.append(0)
        elif patient.hart_rate >= 91 and patient.hart_rate <= 110 or patient.hart_rate >= 41 and patient.hart_rate <= 50:
            self.score.append(1)
        elif patient.hart_rate >= 111 and patient.hart_rate <= 130:
            self.score.append(2)
        elif patient.hart_rate <= 40 or patient.hart_rate >= 131:
            self.score.append(3)

        for self.stat in self.score:
            self.EWS_score += self.stat

        return self.EWS_score
